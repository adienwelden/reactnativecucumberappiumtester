const wd = require("wd");
const assert = require("assert");
const { Before, Given, When, Then, After } = require("cucumber");

const config = require("../../config");

const PORT = 4723;
const driver = wd.promiseChainRemote("localhost", PORT);

Before({ timeout: 50000 }, async () => {
  await driver.init(config);
  await driver.sleep(6000);
  // await driver.launchApp();
});

After(async () => {
  await driver.quit();
});

Given("estou na tela de login", { timeout: 10000 }, async () => {
  let campoEmail = await driver.hasElementByAccessibilityId("campoEmail");
  assert.equal(campoEmail, true);
});

When("digito o email", { timeout: 2000 }, async () => {
  let campoEmail = await driver.elementByAccessibilityId("campoEmail");
  campoEmail.sendKeys("natalia@gmail.com");
});

When("digito a senha", { timeout: 2000 }, async () => {
  let campoSenha = await driver.elementByAccessibilityId("campoSenha");
  campoSenha.sendKeys("masadknnan");
});

When("clico entrar", { timeout: 2000 }, async () => {
  let botaoEntrar = await driver.elementByAccessibilityId("botaoEntrar");
  botaoEntrar.click();
});

Then("entro no app", { timeout: 30000 }, async () => {
  await driver.sleep(10000);
  await driver.setImplicitWaitTimeout(10000);
  let homePage = await driver.hasElementByAccessibilityId("homePage");
  assert.equal(homePage, true);
});
