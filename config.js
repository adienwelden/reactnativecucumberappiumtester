const config = {
  platformName: "Android",
  deviceName: "<emulatorName>",
  automationName: "UiAutomator2",
  appPackage: "<packageName>", // in AndroidManifest.xml
  appActivity: "<packageName>.MainActivity",
  app: "<appProjectDir>/android/app/build/outputs/apk/release/app-release.apk"
};

module.exports = config;
