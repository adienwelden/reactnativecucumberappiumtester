## Cucumber + Appium + Webdriver para React Native (Android)

![Visão geral](/wdio-cucumber-appium.png)

## Descrição 🎯

Ferramenta de teste caixa preta em RN .apk

## Seu projeto 🎯

1. Definir `testId` e `accessibilityLabel` de seus testáveis.
2. Startar seu projeto `npm start`.
3. Criar release do seu projeto `cd android && ./gradlew assembleRelease`.

## Seu teste 🎯

1. Implementar seu teste cucumber em `features`. :writing_hand:
2. `npm install`.
3. Configurar `app`, `deviceName`, `appPackage` e `appActivity` em config.js.
4. Iniciar emulador `emulator -avd <Nome Emulador>`.
5. `npm run appium`.
6. `npm test-android`.
